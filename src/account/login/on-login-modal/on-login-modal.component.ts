import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { AppAuthService } from '@app/shared/common/auth/app-auth.service';
import { LoginService } from '../login.service';
import { DataServiceService } from '../data-service.service';

@Component({
  selector: 'app-on-login-modal',
  templateUrl: './on-login-modal.component.html',
  styleUrls: ['./on-login-modal.component.css']
})
export class OnLoginModalComponent implements OnInit {
  title: string;
  closeBtnName: string;
  list: any[] = ["This is a test modal"];
  isAccepted: string;

  constructor(
    public bsModalRef: BsModalRef,
    public authService: AppAuthService,
    public loginService: LoginService,
    private data: DataServiceService

    ) { }

  ngOnInit() {
    this.title = "Terms and Conditions";
    this.list.push('TEST!!!');
    this.data.currentMessage.subscribe(isAccepted => this.isAccepted = isAccepted)
    
  }

  accept(): void 
  {
    this.isAccepted = "true";
    this.data.changeMessage(this.isAccepted);

  }
}
