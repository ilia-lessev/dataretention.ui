import { AbpSessionService } from '@abp/session/abp-session.service';
import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { accountModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { SessionServiceProxy, UpdateUserSignInTokenOutput } from '@shared/service-proxies/service-proxies';
import { UrlHelper } from 'shared/helpers/UrlHelper';
import { ExternalLoginProvider, LoginService } from './login.service';
import { RecaptchaComponent } from 'ng-recaptcha';
import { AppConsts } from '@shared/AppConsts';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { OnLoginModalComponent } from '@account/login/on-login-modal/on-login-modal.component';
import { DataServiceService } from './data-service.service';

@Component({
    templateUrl: './login.component.html',
    animations: [accountModuleAnimation()],
    styleUrls: ['./login.component.less']
})
export class LoginComponent extends AppComponentBase implements OnInit {
    @ViewChild('recaptchaRef', { static: false }) recaptchaRef: RecaptchaComponent;

    submitting = false;
    isMultiTenancyEnabled: boolean = this.multiTenancy.isEnabled;
    recaptchaSiteKey: string = AppConsts.recaptchaSiteKey;
    captchaResponse?: string;
    bsModalRef: BsModalRef;
    isAccepted: string;

    constructor(
        injector: Injector,
        public loginService: LoginService,
        private _router: Router,
        private _sessionService: AbpSessionService,
        private _sessionAppService: SessionServiceProxy,
        private modalService: BsModalService,
        private data: DataServiceService
    ) {
        super(injector);
    }

    get multiTenancySideIsTeanant(): boolean {
        return this._sessionService.tenantId > 0;
    }

    get isTenantSelfRegistrationAllowed(): boolean {
        return this.setting.getBoolean('App.TenantManagement.AllowSelfRegistration');
    }

    get isSelfRegistrationAllowed(): boolean {
        if (!this._sessionService.tenantId) {
            return false;
        }

        return this.setting.getBoolean('App.UserManagement.AllowSelfRegistration');
    }

    ngOnInit(): void {
        
        this.data.currentMessage.subscribe(isAccepted =>{
            if (isAccepted == "true"){
                this.proceedToLogin();
            }
        });
        if (this._sessionService.userId > 0 && UrlHelper.getReturnUrl() && UrlHelper.getSingleSignIn()) {
            this._sessionAppService.updateUserSignInToken()
                .subscribe((result: UpdateUserSignInTokenOutput) => {
                    const initialReturnUrl = UrlHelper.getReturnUrl();
                    const returnUrl = initialReturnUrl + (initialReturnUrl.indexOf('?') >= 0 ? '&' : '?') +
                        'accessToken=' + result.signInToken +
                        '&userId=' + result.encodedUserId +
                        '&tenantId=' + result.encodedTenantId;

                    location.href = returnUrl;
                });
        }

        let state = UrlHelper.getQueryParametersUsingHash().state;
        if (state && state.indexOf('openIdConnect') >= 0) {
            this.loginService.openIdConnectLoginCallback({});
        }
    }

    login(): void {
        
        if (this.useCaptcha && !this.captchaResponse) {
            this.message.warn(this.l('CaptchaCanNotBeEmpty'));
            return;
        }

        this.modalService.config.backdrop = "static";
        this.modalService.config.backdrop = true;
        this.bsModalRef = this.modalService.show(OnLoginModalComponent);
    
    }

    proceedToLogin() {
        
        this.spinnerService.show();
        this.bsModalRef.hide();
        this.submitting = true;
        this.loginService.authenticate(
            () => {
                this.submitting = false;
                this.spinnerService.hide();
             
                if (this.recaptchaRef) {
                    this.recaptchaRef.reset();
                }
            },
            null,
            this.captchaResponse
        );
    }   

    externalLogin(provider: ExternalLoginProvider) {
        this.loginService.externalAuthenticate(provider);
    }

    get useCaptcha(): boolean {
        return this.setting.getBoolean('App.UserManagement.UseCaptchaOnLogin');
    }

    captchaResolved(captchaResponse: string): void {
        this.captchaResponse = captchaResponse;
    }
}
