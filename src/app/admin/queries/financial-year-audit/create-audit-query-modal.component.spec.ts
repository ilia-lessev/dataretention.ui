import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateAuditQueryModalComponent } from './create-audit-query-modal.component';

describe('CreateAuditQueryModalComponent', () => {
  let component: CreateAuditQueryModalComponent;
  let fixture: ComponentFixture<CreateAuditQueryModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateAuditQueryModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateAuditQueryModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
