import { Component, OnInit, ViewChild, Injector } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
  selector: 'createAuditQueryModal',
  templateUrl: './create-audit-query-modal.component.html',
  styleUrls: ['./create-audit-query-modal.component.css']
})
export class CreateAuditQueryModalComponent extends AppComponentBase implements OnInit {

  @ViewChild('createAuditQueryModal' , { static: true }) createAuditQueryModal: ModalDirective;

  saving = false;

  constructor(injector : Injector) {
    super(injector);
  }

  ngOnInit() {
  }
  
  show():void {
    this.createAuditQueryModal.show();
  }

  save():void {
    //TODO: Not implemented
  }

  close():void{
    this.createAuditQueryModal.hide();
  }
}
