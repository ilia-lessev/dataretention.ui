import { Component, OnInit, ViewChild, Injector } from '@angular/core';
import { CreateAuditQueryModalComponent } from './create-audit-query-modal.component';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
  selector: 'app-financial-year-audit',
  templateUrl: './financial-year-audit.component.html',
  styleUrls: ['./financial-year-audit.component.css']
})
export class FinancialYearAuditComponent extends AppComponentBase implements OnInit {

  @ViewChild(CreateAuditQueryModalComponent, {static:true}) createAuditQueryModal :CreateAuditQueryModalComponent;

  filterText: '';

  constructor(injector: Injector) { 
    super(injector);
  }

  ngOnInit() {
  }

  getQueries(): void {
    //TODO: Not implemented
  }

  createAuditQuery():void {
    this.createAuditQueryModal.show();
  }
}
