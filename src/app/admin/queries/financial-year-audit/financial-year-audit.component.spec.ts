import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinancialYearAuditComponent } from './financial-year-audit.component';

describe('FinancialYearAuditComponent', () => {
  let component: FinancialYearAuditComponent;
  let fixture: ComponentFixture<FinancialYearAuditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinancialYearAuditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinancialYearAuditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
