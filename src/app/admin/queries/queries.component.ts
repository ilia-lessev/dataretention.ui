import { Component, OnInit, Injector, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { QueryServiceProxy, QueryListDto, ListResultDtoOfQueryListDto } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';
import { CreateQueryTicketModalComponent } from './create-query-ticket-modal.component';

@Component({
  selector: 'app-queries',
  templateUrl: './queries.component.html',
  styleUrls: ['./queries.component.css']
})
export class QueriesComponent extends AppComponentBase implements OnInit {
  @ViewChild(CreateQueryTicketModalComponent, { static: true }) createQueryTicketModal: CreateQueryTicketModalComponent;
 
  queries: QueryListDto[] = [];
  advancedFiltersAreShown: false;
  filterText: '';

  constructor(injector: Injector, private _queryService: QueryServiceProxy,) { 
    super(injector);
  }

  ngOnInit(): void {
    this.getQueries();
  }

  getQueries(): void {
    this._queryService.getQueries().pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator())).subscribe((result) => {
      this.primengTableHelper.records = result.items;
      this.primengTableHelper.hideLoadingIndicator();
      this.queries = result.items;
    })
  }

  createQueryTicket():void{
    this.createQueryTicketModal.show();    
  }
}
