import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateQueryTicketModalComponent } from './create-query-ticket-modal.component';

describe('CreateQueryTicketModalComponent', () => {
  let component: CreateQueryTicketModalComponent;
  let fixture: ComponentFixture<CreateQueryTicketModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateQueryTicketModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateQueryTicketModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
