import { Component, ViewChild, Injector, Output, EventEmitter, OnInit } from '@angular/core';
import {FormGroup, Validators, FormBuilder} from '@angular/forms'
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';
import {QueryServiceProxy, DepartmentListDto, QueryCreateDto, QueryTypeListDto} from '@shared/service-proxies/service-proxies';
import {QueriesComponent} from './queries.component';

@Component({  
  selector: 'createQueryTicketModal',
  templateUrl: './create-query-ticket-modal.component.html',
  styleUrls: ['./create-query-ticket-modal.component.css']
})
export class CreateQueryTicketModalComponent extends AppComponentBase implements OnInit {

  @ViewChild('createTicketModal' , { static: true }) createTicketModal: ModalDirective;
   
  departments : DepartmentListDto[] = [];
  department : DepartmentListDto = new DepartmentListDto();  
  query : QueryCreateDto = new QueryCreateDto();  
  statusList: string[] = [];
  fileNames: string[] = [];
  queryTypes : QueryTypeListDto[] = [];
  queryType : QueryTypeListDto = new QueryTypeListDto();
  queriesComponent : QueriesComponent;
  queryForm : FormGroup;
  submitted : boolean = false;
  saving = false;

  constructor(injector: Injector, private _queryService: QueryServiceProxy, private formBuilder : FormBuilder) { 
    super(injector);    
  }
    
  ngOnInit(){   
    var numericValidation = "^[0-9]+$";
    var emailValidation = "^\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$";

    this.queryForm = this.formBuilder.group({
      department: ['', Validators.required],
      queryType : ['', Validators.required],
      equities : [false],
      bonds : [false],
      moneyMarket :[false],
      derivatives :[false],
      companyName : [''],
      companyRegNum : ['', Validators.required],
      entityNumber : ['', Validators.pattern(numericValidation)],
      cashAccNumber : ['', [Validators.pattern(numericValidation), Validators.required]],
      securityAccNumber : ['', [Validators.pattern(numericValidation), Validators.required]],
      contactPerson : [''],
      contactNumber : ['', Validators.pattern(numericValidation)],
      email: ['', Validators.pattern(emailValidation)],
      queryDescription : [''],
      queryStatus :[''],
      assignee : [''],
      queryReference : ['', Validators.pattern(numericValidation)],
      sla : [''],
      dateFrom : ['', Validators.required],
      dateTo : ['', Validators.required]
    });
 }
 
  get f(){return this.queryForm.controls;}

  injector : Injector;
  queryService: QueryServiceProxy;
 
  show():void{
    this.createTicketModal.show();    
    this.getDepartmentList();   
    this.getStatusList();
    this.initialiseQuery();   
  }

  initialiseQuery() : void{
    this.query.queryStatus = "New";
    this.query.assignedTo = "Unassigned";  
  }

  close():void{
    this.createTicketModal.hide();    
    this.queryForm.reset();
  }  

  getDepartmentList():void {        
     this._queryService.getDepartments() 
        .subscribe((result) => {
          this.departments = result.items;          
        });    
  }

  getQueryTypeList(departmentSelected :string): void{     
    console.log(departmentSelected);
     this.query.department = departmentSelected;     
     this._queryService.getQueryTypes(departmentSelected)
          .subscribe((result) => {
             this.queryTypes = result.items;
          });
  }
  
  getStatusList():void{
     this.statusList = ['New','In Progress','Unresolved','Closed']
  }

  getFileDetails(event){
    this.query.attachmentNames = new Array();
    for(var i=0; i<event.target.files.length; i++){
       this.fileNames.push(event.target.files[i].name);    
       this.query.attachmentNames.push(event.target.files[i].name);       
    }
  }
  
  refreshParent(): void {
    location.reload();
  } 

  save():void{  
       this.submitted = true;
       if(this.queryForm.invalid){     
         this.notify.error("Some fields cannot be blank");    
       }
       else{      
        this._queryService.createQuery(this.query)
        .subscribe(() => {
          this.notify.success("Query successfully created");
          this.close();
          this.refreshParent();    
        });    
       }       
  }
}
