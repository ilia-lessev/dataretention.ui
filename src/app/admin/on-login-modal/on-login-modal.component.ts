import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { AppAuthService } from '@app/shared/common/auth/app-auth.service';

@Component({
  selector: 'app-on-login-modal',
  templateUrl: './on-login-modal.component.html',
  styleUrls: ['./on-login-modal.component.css']
})
export class OnLoginModalComponent implements OnInit {
  title: string;
  closeBtnName: string;
  list: any[] = ["This is a test modal"];
  
  constructor(
    public bsModalRef: BsModalRef,
    public authService: AppAuthService) { }

  ngOnInit() {
    this.title = "Terms and Conditions";
    this.list.push('TEST!!!');
  }

}
