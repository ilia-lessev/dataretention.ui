import { ListOperationsDematRematComponent } from './client-instructions/list-operations-demat-remat/list-operations-demat-remat.component';
import { PositionComponent } from './client-instructions/position/position.component';
import { MoneyMarketComponent } from "./client-instructions/money-market/money-market.component";
import { EquityComponent } from "./client-instructions/equity/equity.component";
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { BondsComponent } from "./client-instructions/bonds/bonds.component";

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: "",
                children: [
                    {
                        path: "dashboard",
                        component: DashboardComponent,
                        data: { permission: "Pages.Tenant.Dashboard" }
                    },
                    {
                        path: "money-market",
                        component: MoneyMarketComponent
                    },
                    {
                        path: "equity",
                        component: EquityComponent
                    },
                    {
                        path: "bonds",
                        component: BondsComponent
                    },
                    {
                        path: "position",
                        component: PositionComponent
                    },
                    {
                        path: "list-operations-demat-remat",
                        component: ListOperationsDematRematComponent
                    }
                ]
            }
        ])
    ],
    exports: [RouterModule]
})
export class MainRoutingModule {}
