import { Component, OnInit, ViewChild, Output, EventEmitter, Input, Injector } from '@angular/core';
import { MegaraServiceProxy, DataPagingInput, DematRematOperationsParams, DematRematOperationsInput, IDematRematOperationsOutput } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';
import { FormGroup, FormBuilder } from '@angular/forms';
import * as moment from 'moment';
import { finalize } from 'rxjs/operators';

export interface ListOperationsDematRematModalArgs {
  clientInstructions: IDematRematOperationsOutput[],
  criteria: DematRematOperationsParams,
  rows: number,
}

@Component({
  selector: 'listOperationsDematRematModal',
  templateUrl: './list-operations-demat-remat-modal.component.html',
  styles: []
})
export class ListOperationsDematRematModalComponent extends AppComponentBase implements OnInit {

  @ViewChild('listOperationsDematRematModal', {static:true}) listOperationsDematRematModal: ModalDirective;
  @Output() resultsReceived: EventEmitter<ListOperationsDematRematModalArgs> = new EventEmitter<ListOperationsDematRematModalArgs>();
  @Input() pageSize: number;

  criteriaForm: FormGroup
  clientInstructionTotal = 0;
  isLoading = false;

  constructor(injector: Injector, private _megaraService: MegaraServiceProxy, private formBuilder: FormBuilder) {
    super(injector);
  }

  ngOnInit() {
    this.criteriaForm = this.formBuilder.group({
      order_ID: [''],
      client_Ref: [''],
      app_Ref: [''],
      client: [''],
      sec_Acc: [''],
      trans_Type: [''],
      fI: [''],
      quantity: [''],
      off_Ex: [''],
      physical_Leg_Status: [''],
      electronic_Leg_Status:[''],
      nostro_Sec_Acc: [''],
      trade_Date: [''],
      trade_Date_End: [''],
      sett_Date: [''],
      sett_Date_End: [''],
      taxable: [''],
      creation_Date: [''],
      update_Date: [''],
      creation_Date_End: [''],
      update_Date_End: [''],
      asset_Family: [''],
      creator_User_ID: [''],
      update_User_ID: [''],
      custodian: [''],
    });
  }

  show(): void {
    this.listOperationsDematRematModal.show();
  }

  hide(): void {
    this.listOperationsDematRematModal.hide();
  }

  cancel():void {
    this.hide();
  }

  processForm():void {
    let params = this.criteriaForm.value;
    Object.keys(params).forEach(key => {
      let value = params[key];
      if (key.includes('Date')) {
        params[key] = (value && value !== '') ? moment(value) : null;
        return;
      }

      params[key] = value;
    });

    let queryParams = new DematRematOperationsParams({...params});

    this.isLoading = true;
    this._megaraService.countDematRematOperations(queryParams).subscribe((rows) => {
      this.fetchClientInstructions(queryParams, rows);
    }, () => {
      this.isLoading = false;
    });
  }

  fetchClientInstructions(queryParams: DematRematOperationsParams, rows: number) {
    let paging = new DataPagingInput({pageNumber: 1, pageSize: this.pageSize});
    let body = new DematRematOperationsInput({paging, queryParams});
    this._megaraService.dematRematOperations(body).pipe(finalize(() => this.isLoading = false)).subscribe((result) => {
      this.resultsReceived.emit({clientInstructions: result.items, criteria: queryParams, rows});
      this.hide();
    });
  }
}
