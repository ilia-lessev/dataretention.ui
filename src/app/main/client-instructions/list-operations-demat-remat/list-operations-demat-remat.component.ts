import { MegaraServiceProxy, DataPagingInput, DematRematOperationsInput, DematRematOperationsParams } from '@shared/service-proxies/service-proxies';
import { ListOperationsDematRematModalComponent, ListOperationsDematRematModalArgs } from './list-operations-demat-remat-modal/list-operations-demat-remat-modal.component';
import { Component, ViewChild, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { Paginator } from 'primeng/paginator';
import { objectPropsToColumns } from '../utils';
import { appModuleAnimation } from '@shared/animations/routerTransition';

@Component({
  selector: 'app-list-operations-demat-remat',
  templateUrl: './list-operations-demat-remat.component.html',
  styles: [],
  animations: [appModuleAnimation()],
})
export class ListOperationsDematRematComponent extends AppComponentBase {

  @ViewChild(ListOperationsDematRematModalComponent, {static:true}) listOperationsDematRematModal : ListOperationsDematRematModalComponent;
  @ViewChild('clientInstructionPaginator', { static: true }) clientInstructionPaginator: Paginator;

  clientInstructionsCriteria: DematRematOperationsParams;
  totalClientInstructionsCount: number;
  pageSize: number = 10;
  tableColumns = [];

  constructor(injector: Injector,  private _megaraService: MegaraServiceProxy) {
    super(injector);
  }

  loadInitialResults(data: ListOperationsDematRematModalArgs): void {
    this.totalClientInstructionsCount = data.rows;
    this.clientInstructionsCriteria = data.criteria;
    this.primengTableHelper.records = data.clientInstructions;
    if(data.clientInstructions) {
      this.tableColumns = objectPropsToColumns(data.clientInstructions[0]);
    }
  }

  paginate(data: any):  void {
    if(data.rows) {
        this.pageSize = data.rows;
    }

    this.fetchClientInstructionsForPage(++data.page);
  }

  fetchClientInstructionsForPage(pageNumber: number) {
    let paging = new DataPagingInput({pageNumber, pageSize: this.pageSize});
    let body = new DematRematOperationsInput({paging, queryParams: this.clientInstructionsCriteria});
    this.primengTableHelper.showLoadingIndicator();
    this._megaraService.dematRematOperations(body).subscribe((result) => {
      this.primengTableHelper.records = result.items;
      this.primengTableHelper.hideLoadingIndicator();
    });
  }
}