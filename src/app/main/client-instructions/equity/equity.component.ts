import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Paginator } from 'primeng/components/paginator/paginator';
import { EquityCriteriaModalComponent, EquityCriteriaModalArgs } from './equity-criteria-modal/equity-criteria-modal.component';
import { AppComponentBase } from "shared/common/app-component-base";
import { Component, Injector, ViewChild } from "@angular/core";
import { ClientInstructionsEquityParams, DataPagingInput, ClientInstructionsEquityInput, MegaraServiceProxy } from '@shared/service-proxies/service-proxies';
import { objectPropsToColumns } from '../utils';

@Component({
    selector: "app-equity",
    templateUrl: "./equity.component.html",
    styles: [],
    animations: [appModuleAnimation()]
})
export class EquityComponent extends AppComponentBase {

  @ViewChild(EquityCriteriaModalComponent, {static:true}) equityCriteriaModal : EquityCriteriaModalComponent;
  @ViewChild('clientInstructionPaginator', { static: true }) clientInstructionPaginator: Paginator;

  clientInstructionsCriteria: ClientInstructionsEquityParams;
  totalClientInstructionsCount: number;
  pageSize: number = 10;
  tableColumns = [];

  constructor(injector: Injector,  private _megaraService: MegaraServiceProxy) {
    super(injector);
  }

  loadInitialResults(data: EquityCriteriaModalArgs): void {
    this.totalClientInstructionsCount = data.rows;
    this.clientInstructionsCriteria = data.criteria;
    this.primengTableHelper.records = data.clientInstructions;
    if(data.clientInstructions) {
      this.tableColumns = objectPropsToColumns(data.clientInstructions[0]);
    }
  }

  paginate(data: any):  void {
      if(data.rows) {
          this.pageSize = data.rows;
      }

      this.fetchClientInstructionsForPage(++data.page);
  }

  fetchClientInstructionsForPage(pageNumber: number) {
    let paging = new DataPagingInput({pageNumber, pageSize: this.pageSize});
    let body = new ClientInstructionsEquityInput({paging, queryParams: this.clientInstructionsCriteria});
    this.primengTableHelper.showLoadingIndicator();
    this._megaraService.clientInstructionsEquity(body).subscribe((result) => {
        this.primengTableHelper.records = result.items;
        this.primengTableHelper.hideLoadingIndicator();
    });
  }
}