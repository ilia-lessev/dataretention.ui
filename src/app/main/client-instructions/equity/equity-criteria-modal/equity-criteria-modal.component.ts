import { ModalDirective } from 'ngx-bootstrap';
import { AppComponentBase } from 'shared/common/app-component-base';
import { Component, OnInit, Injector, ViewChild, Output, EventEmitter, Input } from '@angular/core';
import { MegaraServiceProxy, ClientInstructionsEquityInput, DataPagingInput, ClientInstructionsEquityParams, IClientInstructionsEquityOutput, IClientInstructionsEquityParams} from '@shared/service-proxies/service-proxies';
import { FormBuilder, FormGroup } from '@angular/forms';
import * as moment from 'moment';
import { finalize } from 'rxjs/operators';

export interface EquityCriteriaModalArgs {
  clientInstructions: IClientInstructionsEquityOutput[],
  criteria: ClientInstructionsEquityParams,
  rows: number,
}

@Component({
  selector: 'equityCriteriaModal',
  templateUrl: './equity-criteria-modal.component.html',
  styles: []
})
export class EquityCriteriaModalComponent extends AppComponentBase implements OnInit {

  @ViewChild('equityCriteriaModal', {static:true}) equityCriteriaModal: ModalDirective;
  @Output() resultsReceived: EventEmitter<EquityCriteriaModalArgs> = new EventEmitter<EquityCriteriaModalArgs>();
  @Input() pageSize: number;

  criteriaForm: FormGroup
  clientInstructionTotal = 0;
  isLoading = false;

  constructor(injector: Injector, private _megaraService: MegaraServiceProxy, private formBuilder: FormBuilder) {
    super(injector);
  }

  ngOnInit() {
    this.criteriaForm = this.formBuilder.group({
      order_ID: [''],
      client_Ref: [''],
      app_Ref: [''],
      client: [''],
      sec_Acc: [''],
      trans_Type: [''],
      fI: [''],
      quantity: [''],
      off_Ex: [''],
      counterpart: [''],
      client_Inst_Status: [''],
      nostro_Sec_Acc: [''],
      trade_Date_Start:[''],
      trade_Date_End: [''],
      sett_Date_Start: [''],
      sett_Date_End:[''],
      taxable: [''],
      jSE_Status: [''],
      sub_Status: [''],
      market: [''],
      payment_Reference: [''],
      creation_Date_Start: [''],
      creation_Date_End: [''],
      update_Date_Start: [''],
      update_Date_End: [''],
      asset_Family: [''],
      creator_User_ID: [''],
      update_User_ID: [''],
      orderQuantity: 0,
    });
  }

  show(): void {
    this.equityCriteriaModal.show();
  }

  hide(): void {
    this.equityCriteriaModal.hide();
  }

  cancel():void {
    this.hide();
  }

  processForm():void {
    let params = this.criteriaForm.value;
    Object.keys(params).forEach(key => {
      let value = params[key];
      if (key.includes('Date')) {
        params[key] = (value && value !== '') ? moment(value) : null;
        return;
      }

      params[key] = value;
    });

    let queryParams = new ClientInstructionsEquityParams({...params});

    this.isLoading = true;
    this._megaraService.countClientInstructionsEquity(queryParams).subscribe((rows) => {
      this.fetchClientInstructions(queryParams, rows);
    }, () => {
      this.isLoading = false;
    });
  }

  fetchClientInstructions(queryParams: ClientInstructionsEquityParams, rows: number) {
    let paging = new DataPagingInput({pageNumber: 1, pageSize: this.pageSize});
    let body = new ClientInstructionsEquityInput({paging, queryParams});
    this._megaraService.clientInstructionsEquity(body).pipe(finalize(() => this.isLoading = false)).subscribe((result) => {
      this.resultsReceived.emit({clientInstructions: result.items, criteria: queryParams, rows});
      this.hide();
    });
  }
}
