export function removeUnderscores(text: string): string {
  if (!text || !text.trim().length) {
    return text;
  }

  return text.toLocaleLowerCase().replace(/_/g, " ");
}

export function capitaliseSentence(text: string): string {
  if (!text || !text.trim().length) {
    return text;
  }

  return text.split(" ")
    .map(word => {
      return (word && word.length)
        ? word.replace(word[0], word[0].toUpperCase())
        : word;
    }).join(' ');
}

export function objectPropsToColumns(obj: Object): {field: string, header: string}[] {
  return Object.keys(obj).map(key => ({
    field: key,
    header: capitaliseSentence(removeUnderscores(key))
  }));
}