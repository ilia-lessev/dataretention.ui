import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from "shared/common/app-component-base";
import { Component, Injector, ViewChild } from "@angular/core";
import { BondsCriteriaModalComponent, BondsCriteriaModalArgs } from "./bonds-criteria-modal/bonds-criteria-modal.component";
import { Paginator } from "primeng/paginator";
import { MegaraServiceProxy, DataPagingInput, ClientInstructionsBondsParams, ClientInstructionsBondsInput } from "@shared/service-proxies/service-proxies";
import { objectPropsToColumns } from "../utils";

@Component({
    selector: "app-bonds",
    templateUrl: "./bonds.component.html",
    styles: [],
    animations: [appModuleAnimation()]
})
export class BondsComponent extends AppComponentBase {

  @ViewChild(BondsCriteriaModalComponent, {static:true}) bondsCriteriaModal : BondsCriteriaModalComponent;
  @ViewChild('clientInstructionPaginator', { static: true }) clientInstructionPaginator: Paginator;

  clientInstructionsCriteria: ClientInstructionsBondsParams;
  totalClientInstructionsCount: number;
  pageSize: number = 10;
  tableColumns = [];

  constructor(injector: Injector, private _megaraService: MegaraServiceProxy) {
    super(injector);
  }

  loadInitialResults(data: BondsCriteriaModalArgs): void {
    this.totalClientInstructionsCount = data.rows;
    this.clientInstructionsCriteria = data.criteria;
    this.primengTableHelper.records = data.clientInstructions;
    if(data.clientInstructions) {
      this.tableColumns = objectPropsToColumns(data.clientInstructions[0]);
    }
  }

  paginate(data: any):  void {
    if(data.rows) {
        this.pageSize = data.rows;
    }

    this.fetchClientInstructionsForPage(++data.page);
  }

  fetchClientInstructionsForPage(pageNumber: number) {
    let paging = new DataPagingInput({pageNumber, pageSize: this.pageSize});
    let body = new ClientInstructionsBondsInput({paging, queryParams: this.clientInstructionsCriteria});
    this.primengTableHelper.showLoadingIndicator();
    this._megaraService.clientInstructionsBonds(body).subscribe((result) => {
      this.primengTableHelper.records = result.items;
      this.primengTableHelper.hideLoadingIndicator();
    });
  }
}