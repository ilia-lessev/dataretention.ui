import { MegaraServiceProxy, ClientPositionParams, ClientInstructionsClientPositionInput, DataPagingInput, IClientPositionOutput } from '@shared/service-proxies/service-proxies';
import { Component, OnInit, ViewChild, Output, EventEmitter, Input, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';
import { FormGroup, FormBuilder } from '@angular/forms';
import * as moment from 'moment';
import { finalize } from 'rxjs/operators';

export interface PositionCriteriaModalArgs {
  clientInstructions: IClientPositionOutput[],
  criteria: ClientPositionParams,
  rows: number,
}

@Component({
  selector: 'positionCriteriaModal',
  templateUrl: './position-criteria-modal.component.html',
  styles: []
})
export class PositionCriteriaModalComponent extends AppComponentBase implements OnInit {

  @ViewChild('positionCriteriaModal', {static:true}) positionCriteriaModal: ModalDirective;
  @Output() resultsReceived: EventEmitter<PositionCriteriaModalArgs> = new EventEmitter<PositionCriteriaModalArgs>();
  @Input() pageSize: number;

  criteriaForm: FormGroup
  clientInstructionTotal = 0;
  isLoading = false;

  constructor(injector: Injector, private _megaraService: MegaraServiceProxy, private formBuilder: FormBuilder) {
    super(injector);
  }

  ngOnInit() {
    this.criteriaForm = this.formBuilder.group({
      client: [''],
      position_Date: [''],
      position_Date_End: [''],
      origin: [''],
      counterpart_Code: [''],
      sec_Acc: [''],
      impacted: [''],
      tradable_Asset: [''],
      depo: [''],
      quantity!: [''],
      processed: [''],
      position_Nature: [''],
      description: [''],
      proxy: [''],
      asset_Nature: [''],
      main_Reference: [''],
      position_Identifier: [''],
      creation_Date: [''],
      update_Date: [''],
      creation_Date_End: [''],
      update_Date_End!: [''],
    });
  }

  show(): void {
    this.positionCriteriaModal.show();
  }

  hide(): void {
    this.positionCriteriaModal.hide();
  }

  cancel():void {
    this.hide();
  }

  processForm():void {
    let params = this.criteriaForm.value;
    Object.keys(params).forEach(key => {
      let value = params[key];
      if (key.includes('Date')) {
        params[key] = (value && value !== '') ? moment(value) : null;
        return;
      }

      params[key] = value;
    });
    
    let queryParams = new ClientPositionParams({...params});

    this.isLoading = true;
    this._megaraService.countClientPosition(queryParams).subscribe((rows) => {
      this.fetchClientPosition(queryParams, rows);
    }, () => {
      this.isLoading = false;
    });
  }

  fetchClientPosition(queryParams: ClientPositionParams, rows: number) {
    let paging = new DataPagingInput({pageNumber: 1, pageSize: this.pageSize});
    let body = new ClientInstructionsClientPositionInput({paging, queryParams});
    this._megaraService.clientPosition(body).pipe(finalize(() => this.isLoading = false)).subscribe((result) => {
      this.resultsReceived.emit({clientInstructions: result.items, criteria: queryParams, rows});
      this.hide();
    });
  }
}