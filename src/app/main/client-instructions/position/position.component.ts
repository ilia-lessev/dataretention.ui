import { MegaraServiceProxy, ClientPositionParams, DataPagingInput, ClientInstructionsClientPositionInput } from '@shared/service-proxies/service-proxies';
import { PositionCriteriaModalComponent, PositionCriteriaModalArgs } from './position-criteria-modal/position-criteria-modal.component';
import { AppComponentBase } from 'shared/common/app-component-base';
import { Component, ViewChild, Injector } from '@angular/core';
import { Paginator } from 'primeng/paginator';
import { objectPropsToColumns } from '../utils';
import { appModuleAnimation } from '@shared/animations/routerTransition';

@Component({
  selector: 'app-position',
  templateUrl: './position.component.html',
  styles: [],
  animations: [appModuleAnimation()]
})
export class PositionComponent extends AppComponentBase {

  @ViewChild(PositionCriteriaModalComponent, {static:true}) positionCriteriaModal : PositionCriteriaModalComponent;
  @ViewChild('clientInstructionPaginator', { static: true }) clientInstructionPaginator: Paginator;

  clientInstructionsCriteria: ClientPositionParams;
  totalClientInstructionsCount: number;
  pageSize: number = 10;
  tableColumns = [];

  constructor(injector: Injector, private _megaraService: MegaraServiceProxy) {
    super(injector);
  }

  loadInitialResults(data: PositionCriteriaModalArgs): void {
    this.totalClientInstructionsCount = data.rows;
    this.clientInstructionsCriteria = data.criteria;
    this.primengTableHelper.records = data.clientInstructions;
    if(data.clientInstructions) {
      this.tableColumns = objectPropsToColumns(data.clientInstructions[0]);
    }
  }

  paginate(data: any):  void {
    if(data.rows) {
        this.pageSize = data.rows;
    }

    this.fetchClientInstructionsForPage(++data.page);
  }

  fetchClientInstructionsForPage(pageNumber: number) {
    let paging = new DataPagingInput({pageNumber, pageSize: this.pageSize});
    let body = new ClientInstructionsClientPositionInput({paging, queryParams: this.clientInstructionsCriteria});
    this.primengTableHelper.showLoadingIndicator();
    this._megaraService.clientPosition(body).subscribe((result) => {
      this.primengTableHelper.records = result.items;
      this.primengTableHelper.hideLoadingIndicator();
    });
  }
}