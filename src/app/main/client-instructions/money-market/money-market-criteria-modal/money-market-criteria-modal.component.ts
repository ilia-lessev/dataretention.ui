import { Component, OnInit, ViewChild, Output, EventEmitter, Input, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';
import { FormGroup, FormBuilder } from '@angular/forms';
import * as moment from 'moment';
import { MegaraServiceProxy, DataPagingInput, IClientInstructionsMoneyMarketOutput, ClientInstructionsMoneyMarketParams, ClientInstructionsMoneyMarketInput } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';

export interface MoneyMarketCriteriaModalArgs {
  clientInstructions: IClientInstructionsMoneyMarketOutput[],
  criteria: ClientInstructionsMoneyMarketParams,
  rows: number,
}

@Component({
  selector: 'moneyMarketCriteriaModal',
  templateUrl: './money-market-criteria-modal.component.html',
  styles: []
})
export class MoneyMarketCriteriaModalComponent extends AppComponentBase implements OnInit {

  @ViewChild('moneyMarketCriteriaModal', {static:true}) moneyMarketCriteriaModal: ModalDirective;
  @Output() resultsReceived: EventEmitter<MoneyMarketCriteriaModalArgs> = new EventEmitter<MoneyMarketCriteriaModalArgs>();
  @Input() pageSize: number;

  criteriaForm: FormGroup
  clientInstructionTotal = 0;
  isLoading = false;

  constructor(injector: Injector, private _megaraService: MegaraServiceProxy, private formBuilder: FormBuilder) {
    super(injector);
  }

  ngOnInit() {
    this.criteriaForm = this.formBuilder.group({
      main_Reference: [''],
      client_Ref: [''],
      app_Ref: [''],
      client: [''],
      sec_Acc: [''],
      trans_Type: [''],
      fI: [''],
      quantity: [''],
      counterpart: [''],
      client_Inst_Status: [''],
      nostro_Sec_Acc: [''],
      trade_Date: [''],
      trade_Date_End: [''],
      sett_Date: [''],
      sett_Date_End: [''],
      creation_Date: [''],
      creation_Date_End: [''],
      update_Date: [''],
      update_Date_End: [''],
      creator_User_ID: [''],
      update_User_ID: [''],
      custodian: [''],
    });
  }

  show(): void {
    this.moneyMarketCriteriaModal.show();
  }

  hide(): void {
    this.moneyMarketCriteriaModal.hide();
  }

  cancel():void {
    this.hide();
  }

  processForm():void {
    let params = this.criteriaForm.value;
    Object.keys(params).forEach(key => {
      let value = params[key];
      if (key.includes('Date')) {
        params[key] = (value && value !== '') ? moment(value) : null;
        return;
      }

      params[key] = value;
    });

    let queryParams = new ClientInstructionsMoneyMarketParams({...params});

    this.isLoading = true;
    this._megaraService.countClientInstructionsMoneyMarket(queryParams).subscribe((rows) => {
      this.fetchClientInstructions(queryParams, rows);
    }, () => {
      this.isLoading = false;
    });
  }

  fetchClientInstructions(queryParams: ClientInstructionsMoneyMarketParams, rows: number) {
    let paging = new DataPagingInput({pageNumber: 1, pageSize: this.pageSize});
    let body = new ClientInstructionsMoneyMarketInput({paging, queryParams});
    this._megaraService.clientInstructionsMoneyMarket(body).pipe(finalize(() => this.isLoading = false)).subscribe((result) => {
      this.resultsReceived.emit({clientInstructions: result.items, criteria: queryParams, rows});
      this.hide();
    });
  }
}
