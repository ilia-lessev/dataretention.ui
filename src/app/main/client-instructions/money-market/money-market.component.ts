import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from "shared/common/app-component-base";
import { Component, Injector, ViewChild } from "@angular/core";
import { MoneyMarketCriteriaModalComponent, MoneyMarketCriteriaModalArgs } from "./money-market-criteria-modal/money-market-criteria-modal.component";
import { Paginator } from "primeng/paginator";
import { MegaraServiceProxy, DataPagingInput, ClientInstructionsMoneyMarketParams, ClientInstructionsMoneyMarketInput } from "@shared/service-proxies/service-proxies";
import { objectPropsToColumns } from "../utils";

@Component({
    selector: "app-money-market",
    templateUrl: "./money-market.component.html",
    styles: [],
    animations: [appModuleAnimation()]
})
export class MoneyMarketComponent extends AppComponentBase {

  @ViewChild(MoneyMarketCriteriaModalComponent, {static:true}) moneyMarketCriteriaModal : MoneyMarketCriteriaModalComponent;
  @ViewChild('clientInstructionPaginator', { static: true }) clientInstructionPaginator: Paginator;

  clientInstructionsCriteria: ClientInstructionsMoneyMarketParams;
  totalClientInstructionsCount: number;
  pageSize: number = 10;
  tableColumns = [];

  constructor(injector: Injector,  private _megaraService: MegaraServiceProxy) {
    super(injector);
  }

  loadInitialResults(data: MoneyMarketCriteriaModalArgs): void {
    this.totalClientInstructionsCount = data.rows;
    this.clientInstructionsCriteria = data.criteria;
    this.primengTableHelper.records = data.clientInstructions;
    if(data.clientInstructions) {
      this.tableColumns = objectPropsToColumns(data.clientInstructions[0]);
    }
  }

  paginate(data: any):  void {
    if(data.rows) {
      this.pageSize = data.rows;
    }

    this.fetchClientInstructionsForPage(++data.page);
  }

  fetchClientInstructionsForPage(pageNumber: number) {
    let paging = new DataPagingInput({pageNumber, pageSize: this.pageSize});
    let body = new ClientInstructionsMoneyMarketInput({paging, queryParams: this.clientInstructionsCriteria});
    this.primengTableHelper.showLoadingIndicator();
    this._megaraService.clientInstructionsMoneyMarket(body).subscribe((result) => {
      this.primengTableHelper.records = result.items;
      this.primengTableHelper.hideLoadingIndicator();
    });
  }
}