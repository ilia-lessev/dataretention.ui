import { PaginatorModule } from 'primeng/paginator';
import { TableModule } from 'primeng/table';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppCommonModule } from '@app/shared/common/app-common.module';
import { UtilsModule } from '@shared/utils/utils.module';
import { CountoModule } from 'angular2-counto';
import { ModalModule, TabsModule, TooltipModule, BsDropdownModule, PopoverModule } from 'ngx-bootstrap';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MainRoutingModule } from './main-routing.module';
import { NgxChartsModule } from '@swimlane/ngx-charts';

import { BsDatepickerModule, BsDatepickerConfig, BsDaterangepickerConfig, BsLocaleService } from 'ngx-bootstrap/datepicker';
import { NgxBootstrapDatePickerConfigService } from 'assets/ngx-bootstrap/ngx-bootstrap-datepicker-config.service';
import { EquityComponent } from './client-instructions/equity/equity.component';
import { BondsComponent } from './client-instructions/bonds/bonds.component';
import { MoneyMarketComponent } from './client-instructions/money-market/money-market.component';
import { EquityCriteriaModalComponent } from './client-instructions/equity/equity-criteria-modal/equity-criteria-modal.component';
import { PositionComponent } from './client-instructions/position/position.component';
import { PositionCriteriaModalComponent } from './client-instructions/position/position-criteria-modal/position-criteria-modal.component';
import { BondsCriteriaModalComponent } from './client-instructions/bonds/bonds-criteria-modal/bonds-criteria-modal.component';
import { MoneyMarketCriteriaModalComponent } from './client-instructions/money-market/money-market-criteria-modal/money-market-criteria-modal.component';
import { ListOperationsDematRematComponent } from './client-instructions/list-operations-demat-remat/list-operations-demat-remat.component';
import { ListOperationsDematRematModalComponent } from './client-instructions/list-operations-demat-remat/list-operations-demat-remat-modal/list-operations-demat-remat-modal.component';

NgxBootstrapDatePickerConfigService.registerNgxBootstrapDatePickerLocales();

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        ModalModule,
        TabsModule,
        TooltipModule,
        TableModule,
        PaginatorModule,
        AppCommonModule,
        UtilsModule,
        MainRoutingModule,
        CountoModule,
        NgxChartsModule,
        BsDatepickerModule.forRoot(),
        BsDropdownModule.forRoot(),
        PopoverModule.forRoot()
    ],
    declarations: [
        DashboardComponent,
        EquityComponent,
        BondsComponent,
        MoneyMarketComponent,
        EquityCriteriaModalComponent,
        PositionComponent,
        PositionCriteriaModalComponent,
        BondsCriteriaModalComponent,
        MoneyMarketCriteriaModalComponent,
        ListOperationsDematRematComponent,
        ListOperationsDematRematModalComponent  
    ],
    providers: [
        { provide: BsDatepickerConfig, useFactory: NgxBootstrapDatePickerConfigService.getDatepickerConfig },
        { provide: BsDaterangepickerConfig, useFactory: NgxBootstrapDatePickerConfigService.getDaterangepickerConfig },
        { provide: BsLocaleService, useFactory: NgxBootstrapDatePickerConfigService.getDatepickerLocale }
    ]
})
export class MainModule { }
